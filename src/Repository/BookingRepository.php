<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }


    public function findBookedHabitatsByUser($email)
    {
        return $this->createQueryBuilder('b')
            ->where('b.tenant = :tenant')
            ->andWhere('b.bookend > CURRENT_DATE()')
            ->setParameter('tenant', $email)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findBookedHabitats($habitat_id)
    {
        return $this->createQueryBuilder('b')
            ->where('b.habitat = :habitat')
            ->andWhere('b.bookend > CURRENT_DATE()')
            ->setParameter('habitat', $habitat_id)
            ->getQuery()
            ->getResult();
    }
}

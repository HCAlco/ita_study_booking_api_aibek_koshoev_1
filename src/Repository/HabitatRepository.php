<?php

namespace App\Repository;

use App\Entity\Habitat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Habitat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Habitat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Habitat[]    findAll()
 * @method Habitat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HabitatRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Habitat::class);
    }

    public function findHabitatByFilters($title, int $min, int $max, $type)
    {
        $query =  $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.title LIKE :title')
            ->andWhere('c.price BETWEEN :min AND :max');
            if($type){
                $query
                ->andWhere('c.type = :type')
                ->setParameter('type', $type);
            }
            return $query
            ->setParameter('title', '%'.$title.'%')
            ->setParameter('min',$min)
            ->setParameter('max', $max)
            ->getQuery()
            ->getResult();

    }

    public function findHabitatsByPrice($price)
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.price <= :price')
            ->setParameter('price', $price)
            ->getQuery()
            ->getResult();

    }

}

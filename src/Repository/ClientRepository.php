<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOneByPassportAndEmail($email, $passport)
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.passport = :passport')
                ->orWhere('c.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmailAndPassword($email, $password)
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.email = :email')
                ->andWhere('u.password = :password')
                ->setParameter('email', $email)
                ->setParameter('password', $password)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmail($email)
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.email = :value')->setParameter('value', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByGoogleFacebookOrVk($id)
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->where('c.google = :id')
                ->orWhere('c.facebook = :id')
                ->orWhere('c.vkontakte = :id')
                ->setParameter('id', $id)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}

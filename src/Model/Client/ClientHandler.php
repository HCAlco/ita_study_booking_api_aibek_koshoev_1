<?php

namespace App\Model\Client;

use App\Entity\Client;
use App\Entity\Landlord;
use App\Entity\Tenant;

class ClientHandler
{
    /**
     * @param array $data
     * @param bool $encodePassword
     * @return Client
     */
    public function createNewClient(array $data, bool $encodePassword = true) {
        $client = new Client();
        $this->createNewAbstractClient($client, $data, $encodePassword);

        return $client;
    }

    /**
     * @param Client $client
     * @param array $data
     * @param bool $encodePassword
     * @return Client
     */
    public function createNewAbstractClient(Client $client, array $data, bool $encodePassword = true) {
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setVkontakte($data['vkId']??null);
        $client->setFaceBook($data['faceBookId']??null);
        $client->setGoogle($data['googleId']??null);
        $client->setRoles($data['roles']);

        if($encodePassword) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $client->setPassword($password);

        return $client;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return md5($password) . md5($password . '2');
    }
}

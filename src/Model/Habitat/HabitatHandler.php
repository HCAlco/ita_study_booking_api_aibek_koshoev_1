<?php

namespace App\Model\Habitat;


use App\Entity\Booking;
use App\Entity\Cottage;
use App\Entity\Habitat;
use App\Entity\Pension;

class HabitatHandler
{

    /**
     * @param $data
     * @return Booking
     */
    public function createNewBooking(array $data) {
        $habitat = new Booking();
        $habitat->setNumber($data['number']);
        $habitat->setHabitat($data['habitat']);
        $habitat->setTenant($data['tenant']);

        return $habitat;
    }


    /**
     * @param Habitat $habitat
     * @param array $data
     * @return Habitat
     */
    public function createNewAbstractHabitat(Habitat $habitat, array $data) {
        $habitat->setTitle($data['title']);
        $habitat->setType($data['type']);
        $habitat->setRoomqty($data['roomqty']);
        $habitat->setOwner($data['owner']);
        $habitat->setPhone($data['phone']);
        $habitat->setPrice($data['price']);
        $habitat->setAddress($data['address']);

        return $habitat;
    }

    /**
     * @param $data
     * @return Habitat
     */
    public function createNewHabitat(array $data) {
        $habitat = new Habitat();
        $habitat = $this->createNewAbstractHabitat($habitat, $data);

        return $habitat;
    }

    /**
     * @param $data
     * @return Habitat
     */
    public function createNewPension(array $data) {
        /** @var Pension $habitat */
        $habitat = $this->createNewAbstractHabitat(new Pension(), $data);
        $habitat->setRestaurants($data['restaurants']);
        $habitat->setSlippers($data['slippers']);

        return $habitat;
    }

    /**
     * @param $data
     * @return Habitat
     */
    public function createNewCottage(array $data) {
        /** @var Cottage $habitat */
        $habitat = $this->createNewAbstractHabitat(new Cottage(), $data);
        $habitat->setBathrobes($data['bathrobes']);
        $habitat->setBathrooms($data['bathrooms']);

        return $habitat;
    }

}

<?php

namespace App\Controller;

use App\Model\Habitat\HabitatHandler;
use App\Repository\BookingRepository;
use App\Repository\ClientRepository;
use App\Repository\HabitatRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/habitat")
 */
class HabitatController extends Controller
{
    /**
     * @Route("/register", name="app_habitat_register")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @param HabitatHandler $habitatHandler
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function habitatRegisterAction(
        Request $request,
        HabitatHandler $habitatHandler,
        ClientRepository $clientRepository)
    {
        $data['title'] = $request->get('title');
        $data['type'] = $request->get('type');
        $data['roomqty'] = $request->get('roomqty');
        $data['owner'] = $clientRepository->findOneByEmail($request->get('owner'));
        $data['phone'] = $request->get('phone');
        $data['address'] = $request->get('address');
        $data['price'] = $request->get('price');

        if($data['type'] == 'pension'){
            $data['restaurants'] = $request->get('restaurants');
            $data['slippers'] = $request->get('slippers');
            $habitat = $habitatHandler->createNewPension($data);
        }else{
            $data['bathrooms'] = $request->get('bathrooms');
            $data['bathrobes'] = $request->get('bathrobes');
            $habitat = $habitatHandler->createNewCottage($data);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($habitat);
        $em->flush();

        return new JsonResponse(['result' => 'ok']);

    }

    /**
     * @Route("/booking", name="app_habitat_booking")
     * @Method("GET")
     * @param Request $request
     * @param HabitatHandler $habitatHandler
     * @return JsonResponse
     */
    public function registerBookingAction(
        Request $request,
        HabitatHandler $habitatHandler)
    {
        $data['number'] = $request->get('number');
        $data['tenant'] = $request->get('tenant');
        $data['habitat'] = $request->get('habitat');
        $booking = $habitatHandler->createNewBooking($data);
        $em = $this->getDoctrine()->getManager();
        $em->persist($booking);
        $em->flush();


        return new JsonResponse(['result' => $booking->getPrintableBookend()]);

    }

    /**
     * @Route("/getinfo", name="app_habitat_get_info")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getAllHabitatsAction()
    {
        $objects = $this->getDoctrine()->getRepository('App:Habitat')->findAll();
        $habitats = [];
        foreach ($objects as $object) {
            $habitats[] = $object->__toArray();
        }
        return new JsonResponse($habitats);

    }

    /**
     * @Route("/check", name="app_habitat_get_booked_habitats_by_email")
     * @Method("HEAD")
     * @param Request $request
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function getBookedHabitatsByEmailAction(Request $request, BookingRepository $bookingRepository)
    {
        $bookingObject = $bookingRepository->findBookedHabitatsByUser($request->get('email'));
        if(!empty($bookingObject)){
            return new JsonResponse(['result' => 'ok']);
        }else{
            throw new NotFoundHttpException();
        }

    }

    /**
     * @Route("/booked/get", name="app_habitat_get_booked_habitats")
     * @Method("GET")
     * @param Request $request
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function getBookedHabitatsByHabitatAction(Request $request, BookingRepository $bookingRepository)
    {
        $bookingObjects = $bookingRepository->findBookedHabitats($request->get('habitat_id'));
//        dump($bookingObjects);
        if(!empty($bookingObjects)){
            $result = [];
            foreach ($bookingObjects as $room) {
                $temp = $room->__toArray();
                $result[$temp['number']] = $temp['bookend'];
            }
            return new JsonResponse($result);
        }
        return new JsonResponse(false);
    }

    /**
     * @Route("/filters", name="app_habitat_get_habitats_by_filters")
     * @Method("GET")
     * @param Request $request
     * @param HabitatRepository $habitatRepository
     * @return JsonResponse
     */
    public function getHabitatsByFiltersAction(
        Request $request,
        HabitatRepository $habitatRepository)
    {
        $objects = $habitatRepository
            ->findHabitatByFilters(
                $request->get('title'),
                $request->get('min') ?? 0,
                $request->get('max') ?? 1000000,
                $request->get('type'));

        if(!empty($objects)){
            $result = [];
            foreach ($objects as $key => $object){
                $result[] = $object->__toArray();
            }
            return new JsonResponse($result);
        }
        return new JsonResponse(false);

    }
}

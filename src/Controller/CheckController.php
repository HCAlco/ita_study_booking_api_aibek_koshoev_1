<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Tests\Compiler\J;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CheckController extends Controller
{
    /**
     * @Route("/check/{email}/{passport}", name="app_check_user")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @return JsonResponse
     */
    public function clientExistsAction(string $email, string $passport)
    {
        $rep = $this->getDoctrine()->getRepository('App:Client');
        $client = $rep->findOneByPassportAndEmail($email, $passport);
        if(!empty($client)){
            return new JsonResponse(['result' => 'ok']);
        }else{
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/credentials/{email}/{password}", name="app_login_data_check")
     * @Method("GET")
     * @param string $email
     * @param string $password
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function credentialsCheckAction(string $email, string $password, ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByEmailAndPassword($email, $password);
        if(!empty($client)){
            return new JsonResponse($client->toArray());
        }else{
            return new JsonResponse(false);
        }
    }

    /**
     * @Route("/auth/{id}", name="app_check_auth_id")
     * @Method("GET")
     * @param string $id
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkAuthIdAction(string $id, ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByGoogleFacebookOrVk($id);
        if(!empty($client)){
            return new JsonResponse($client->toArray());
        }else{
            return new JsonResponse(false);
        }
    }
}

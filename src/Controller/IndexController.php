<?php

namespace App\Controller;

use App\Model\Client\ClientHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/client/password/encode", name="app_client_password_encode")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function passwordEncodeAction(
        ClientHandler $clientHandler,
        Request $request
    )
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePlainPassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }

    /**
     * @Route("/register", name="app_register")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     */
    public function clientRegisterAction(Request $request, ClientHandler $clientHandler)
    {
        $data['email'] = $request->get('email');
        $data['passport'] = $request->get('passport');
        $data['password'] = $request->get('password');
        $data['google'] = $request->get('google');
        $data['facebook'] = $request->get('facebook');
        $data['vkontakte'] = $request->get('vkontakte');
        $data['roles'] = $request->get('roles');
        if(in_array('ROLE_USER', $data['roles'])){
            unset($data['roles'][0]);
            $data['roles'] = array_values($data['roles']);
        }
        $client = $clientHandler->createNewClient($data);

        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();

        return new JsonResponse(['result' => 'ok']);
    }


}

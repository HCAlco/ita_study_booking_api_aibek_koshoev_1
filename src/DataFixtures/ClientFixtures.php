<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createNewClient([
            'email' => '123@123.ru',
            'passport' => 'passport',
            'password' => '827ccb0eea8a706c4c34a16891f84e7b7692dcdc19e41e66c6ae2de54a696b25', // pass_1234 || 12345
            'full_name' => 'test',
            'roles' => ["ROLE_LANDLORD"]
        ], false);

        $client1 = $this->clientHandler->createNewClient([
            'email' => 'aibek',
            'passport' => 'passportAibek',
            'password' => '81dc9bdb52d04dc20036dbd8313ed055be041b21f66931f5a1d24e1e19a78539',//1234
            'full_name' => 'test',
            'roles' => ["ROLE_TENANT"]
        ], false);

        $manager->persist($client);
        $manager->persist($client1);
        $manager->flush();
    }
}

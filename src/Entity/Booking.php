<?php

namespace App\Entity;

use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tenant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $habitat;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $bookend;

    public function __construct()
    {
        $this->bookend = new \DateTime("now", new DateTimeZone('Asia/Bishkek'));
        $this->bookend->add(new \DateInterval('P7D'));
    }

    public function __toArray(){
        return [
            'number' => $this->number,
            'habitat' => $this->habitat,
            'tenant' => $this->tenant,
            'bookend' => $this->bookend->format('Y.m.d h:m:s')
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return Booking
     */
    public function setNumber(int $number): Booking
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @param mixed $habitat
     * @return Booking
     */
    public function setHabitat($habitat)
    {
        $this->habitat = $habitat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHabitat()
    {
        return $this->habitat;
    }

    /**
     * @param \DateTime $bookend
     * @return Booking
     */
    public function setBookend(\DateTime $bookend): Booking
    {
        $this->bookend = $bookend;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBookend(): \DateTime
    {
        return $this->bookend;
    }

    /**
     * @return string
     */
    public function getPrintableBookend()
    {
        return $this->bookend->format('Y.m.d h:m:s');
    }

    /**
     * @param mixed $tenant
     * @return Booking
     */
    public function setTenant($tenant)
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }
}

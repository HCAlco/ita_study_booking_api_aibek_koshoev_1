<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends Habitat
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $restaurants;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slippers;

    /**
     * @param mixed $slippers
     * @return Pension
     */
    public function setSlippers($slippers)
    {
        $this->slippers = $slippers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlippers()
    {
        return $this->slippers;
    }

    /**
     * @param string $restaurants
     * @return Pension
     */
    public function setRestaurants(string $restaurants)
    {
        $this->restaurants = $restaurants;
        return $this;
    }

    /**
     * @return string
     */
    public function getRestaurants()
    {
        return $this->restaurants;
    }

}

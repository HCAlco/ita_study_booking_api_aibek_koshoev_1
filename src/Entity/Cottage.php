<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends Habitat
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bathrooms;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bathrobes;

    /**
     * @param mixed $bathrooms
     * @return Cottage
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * @param mixed $bathrobes
     * @return Cottage
     */
    public function setBathrobes($bathrobes)
    {
        $this->bathrobes = $bathrobes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBathrobes()
    {
        return $this->bathrobes;
    }

}

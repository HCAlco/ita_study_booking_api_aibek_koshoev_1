<?php

namespace App\Entity;

use DateTimeZone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=254)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $passport;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $google;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $vkontakte;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Habitat", mappedBy="owner")
     */
    private $registeredHabitats;

    public function __construct()
    {
        $this->registeredHabitats = new ArrayCollection();
        $this->registeredAt = new \DateTime("now", new DateTimeZone('Asia/Bishkek'));
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return mixed (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }

    public function addRole(string $role)
    {
        $roles = json_decode($this->roles, true);
        $roles[] = $role;
        $this->roles = json_encode($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param mixed $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return Client
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $username
     * @return Client
     */
    public function setUsername($username)
    {
        $this->email = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return array
     */
    public function toArray(){
        return [
            'email' => $this->email,
            'password' => $this->password,
            'passport' => $this->passport,
            'roles' => $this->getRoles(),
            'google' => $this->google,
            'facebook' => $this->facebook,
            'vkontakte' => $this->vkontakte,
        ];
    }

    /**
     * @param mixed $google
     * @return Client
     */
    public function setGoogle($google)
    {
        $this->google = $google;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogle()
    {
        return $this->google;
    }

    /**
     * @param mixed $facebook
     * @return Client
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $vkontakte
     * @return Client
     */
    public function setVkontakte($vkontakte)
    {
        $this->vkontakte = $vkontakte;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkontakte()
    {
        return $this->vkontakte;
    }
}
